import Vue from 'vue'
import { Button, NavBar, Form, Field, Toast, Search, Tabbar, TabbarItem, Cell, CellGroup, List } from 'vant'

// import这种代码前面不要写注释
// 这个说明Button是vue的插件
Vue.use(Button)
Vue.use(NavBar)

Vue.use(Form)
Vue.use(Field)
Vue.prototype.$toast = Toast

Vue.use(Search)

Vue.use(Tabbar)
Vue.use(TabbarItem)

Vue.use(Cell)
Vue.use(CellGroup)

Vue.use(List)
