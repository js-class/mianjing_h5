// 把获取token,存token 删除token全部封装进来
const KEY = 'vant-mobile-exp-token'
// 存token
export const setToken = (newToken) => {
  localStorage.setItem(KEY, newToken)
}
// 取token
export const getToken = () => {
  return localStorage.getItem(KEY)
}
// 删除token
export const delToken = () => {
  localStorage.removeItem(KEY)
}
