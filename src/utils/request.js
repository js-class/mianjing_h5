// 二次封装的好处
// 1. 让项目和请求的库解耦了 分离了 将来有一天我们想换一个请求库不用axios也可以
// 2. 我没有直接使用axios，我使用的是axios.create
import axios from 'axios'
// axios 和 service 区别是service更加具体
// 有一天我们去了一个大公司的话，可能接口服务器有多台 如果你用的是axios就会有一个问题，axios.default.baseURL这样写
// 就会导致axios只能和这台服务器发请求，别的服务器发不了
// 只有在组件中可以用this.$toast 在js文件中必须自己手动导入一下Toast
import { Toast } from 'vant'
import { getToken, delToken } from '@/utils/storage'
import router from '@/router'
const service = axios.create({
  timeout: 10000,
  baseURL: 'http://interview-api-t.itheima.net/'
})
// const service1 = axios.create({
//     timeout:10000,
//     baseURL:'另一台服务器地址'
// })

// 添加请求拦截器
service.interceptors.request.use(function (config) {
  if (getToken()) {
    config.headers.Authorization = `Bearer ${getToken()}`
  }

  // 在发送请求之前做些什么
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response.data
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  console.log(error.response.data.code)
  if (error.response.data.code === 401) {
    // 把过期的token删除 跳转到登录页
    delToken()
    router.push('/login')
  } else {
    Toast.fail(error.response.data.message)
    return Promise.reject(error)
  }
})
export default service

// axios.default.baseURL
// axios.default.baseURL
// 你在项目中如何使用axios的

// 拦截器可以把一堆的ajax请求一样的代码提取出来
