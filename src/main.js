import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './utils/vant'

import ArticleItem from '@/components/ArticleItem.vue'
Vue.component(ArticleItem.name, ArticleItem)

Vue.config.productionTip = false

// import axios from 'axios'
// Vue.prototype.$axios = axios
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
