import request from '@/utils/request'

/**
 * 获取面经列表
 * @param {*} params
 * @returns
 */
export const getArticles = (params) => {
  return request({
    url: '/h5/interview/query',
    params
    // headers: {
    //   Authorization: `Bearer ${getToken()}`
    // }
  })
}

// 收件人 张三
// 持票人 张三
// 如果每个ajax都要写的就可以在拦截器中写
