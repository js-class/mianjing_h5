import request from '@/utils/request'

/**
 * 注册
 * @param {username,password} data
 * @returns 返回的是promise实例
 */
export const register = data => {
  return request({
    method: 'POST',
    url: '/h5/user/register',
    data
  })
}

/**
 * 登录
 * @param {username,password} data
 * @returns
 */
export const login = data => {
  return request({
    method: 'POST',
    url: '/h5/user/login',
    data
  })
}
