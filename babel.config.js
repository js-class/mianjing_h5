// 凡是带有conig是配置文件 配置文件修改了，一定要重启项目
// 重启vue项目 ctrl + c
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    ['import', {
      libraryName: 'vant',
      libraryDirectory: 'es',
      style: (name) => `${name}/style/less`
    }, 'vant']
  ]
}
